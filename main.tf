terraform {
    required_providers {
        google = {
            source = "hashicorp/google"
            version = "5.14.0"
        }
    }
}

provider "google"{
    project = "myterraformpractice"
    region = ""
    zone = ""
}

resource "google_compute_network" "vpc_network" {
    name = "my-terraform-vpc"
}